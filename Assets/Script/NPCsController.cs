using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCsController : MonoBehaviour
{
    public NavMeshAgent agent;
    public GameObject npc;

    public Vector3 walkPoint1;
    public Vector3 walkPoint2;
    private Vector3 currentPosition;
    private void Awake()
    {
        currentPosition = npc.transform.position;
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(walkPoint1);
    }

    // Update is called once per frame
    void Update()
    {
        if ((npc.transform.position.x == walkPoint1.x) && (npc.transform.position.z == walkPoint1.z))
        {
            // Debug.Log((npc.transform.position.x == walkPoint1.x) && (npc.transform.position.z == walkPoint1.z));
            agent.SetDestination(walkPoint2);
        }
        if((npc.transform.position.x == walkPoint2.x) && (npc.transform.position.z == walkPoint2.z))
        {
            agent.SetDestination(walkPoint1);
        }
    }
}
