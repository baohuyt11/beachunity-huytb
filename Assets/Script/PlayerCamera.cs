using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform target;

    private Vector3 offsetPosition;

    // public Vector3 CameraPosition = GameObject.Find("Eyes").transform.position;

    public Space offsetPositionSpace = Space.Self;

    private bool isView = true;
    public bool lookAt = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        Refresh();
        SwitchView();
    }
    public void SwitchView()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            isView = !isView;
        }
    }
    public void Refresh()
    {
        if (target == null)
        {
            Debug.LogWarning("Missing target ref !", this);

            return;
        }

        if (isView)
        {
            offsetPosition = new Vector3(0, 6F, -10F);
            // compute position
            if (offsetPositionSpace == Space.Self)
            {
                transform.position = target.TransformPoint(offsetPosition);
            }
            else
            {
                transform.position = target.position + offsetPosition;
            }
            if (lookAt)
            {
                transform.LookAt(target);
            }
            else
            {
                transform.rotation = target.rotation;
            }
        }
        else
        {
            offsetPosition = new Vector3(0, 3.5F, 0.15F);
            // compute position
            if (offsetPositionSpace == Space.Self)
            {
                transform.position = target.TransformPoint(offsetPosition);
            }
            else
            {
                transform.position = target.position + offsetPosition;
            }

            transform.rotation = target.rotation;
        }
    }
}
