using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed = 20F;
    private float jumpSpeed = 10F;
    private float gravity = 20.0F;
    // private float rotateSpeed = 10.0F;
    private Vector3 moveDirection = Vector3.zero;

    private Animator playerAnimator;
    private bool isLaying = false;
    private bool isSitGround = false;

    // check isMoving
    private bool isMoving;

    // Time between actions
    private float lastStep, timeBetweenSteps = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Moving();
        SittingAndLaying();
        PlayerControll();
    }
    void Moving()
    {
        // Player speed
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = 50;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = 20;
        }
        // Player move
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            playerAnimator.SetBool("IsWalking", true);
            isMoving = true;
        }

        // Player stop
        if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))
        {
            playerAnimator.SetBool("IsWalking", false);
            isMoving = false;
        }
    }

    void SittingAndLaying()
    {
        // Player isLaying
        if (Input.GetKey(KeyCode.L) && (Time.time - lastStep > timeBetweenSteps) && !isMoving)
        {
            isSitGround = false;
            lastStep = Time.time;
            isLaying = !isLaying;

        }
        playerAnimator.SetBool("IsLaying", isLaying);

        // Player isSitGround
        if (Input.GetKey(KeyCode.G) && (Time.time - lastStep > timeBetweenSteps) && !isMoving)
        {
            isLaying = false;
            lastStep = Time.time;
            isSitGround = !isSitGround;
        }
        playerAnimator.SetBool("IsSitGround", isSitGround);
    }

    void PlayerControll()
    {
        // PlayerController
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded && !isLaying && !isSitGround)
        {
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection.Normalize();

            moveDirection *= speed;
            if (Input.GetButton("Jump")) moveDirection.y = jumpSpeed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        if (!isLaying && !isSitGround) transform.Rotate(0, Input.GetAxis("Horizontal"), 0);
    }
}